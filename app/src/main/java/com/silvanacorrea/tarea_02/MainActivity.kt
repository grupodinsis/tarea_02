package com.silvanacorrea.tarea_02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerfificar.setOnClickListener {
            val annio = edtAnnio.text.toString().toInt()



            when (annio){
                in 1930..1948 -> {
                    tvResultadoGen.text ="Generación Post guerra"
                    tvPoblacion.text = "6 300 000"
                    ivRasgo.setImageResource(R.drawable.austeridad)
                }
                in 1949..1968 -> {
                    tvResultadoGen.text = "Generación BabyBoom"
                    tvPoblacion.text = "12 000 000"
                    ivRasgo.setImageResource(R.drawable.ambicion)
                }
                in 1969..1980 -> {
                    tvResultadoGen.text = "Generación X"
                    tvPoblacion.text = "9 300 000"
                    ivRasgo.setImageResource(R.drawable.obsesion)
                }
                in 1981..1993 -> {
                    tvResultadoGen.text = "Generación Y"
                    tvPoblacion.text = "7 200 000"
                    ivRasgo.setImageResource(R.drawable.frustracion)
                }
                in 1994..2010 -> {
                    tvResultadoGen.text = "Generación Z"
                    tvPoblacion.text = "7 800 000"
                    ivRasgo.setImageResource(R.drawable.irreverencia)
                }
                else -> {
                    tvResultadoGen.text = "No perteneces a ninguna de estas generaciones"
                    tvPoblacion.text = ""
                    ivRasgo.setImageResource(R.drawable.duda)
                }
            }
        }
    }
}